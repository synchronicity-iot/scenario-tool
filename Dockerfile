FROM node:8.4.0

MAINTAINER thomas.gilbert@alexandra.dk

RUN apt-get update && apt-get install vim imagemagick graphicsmagick -y

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install
RUN npm install gulp -g

# Bundle app source
COPY . /usr/src/app

RUN mv config/config.localhost.js   config/config.js && \
    mv config/database.localhost.js config/database.js && \
    mv config/ga.localhost.js       config/ga.js && \
    mv config/auth.localhost.js     config/auth.js

RUN gulp build

ENV WEB_HOST="http://localhost" \
    WEB_PORT=80 \
    WEB_CONTEXTPATH="/" \
    WEB_DEV=false \
    WEB_TITLE="SynchroniCity Scenario Tool"

ENV MONGODB_URL="mongodb://database/scenarios" \
    MONGODB_TEST_URL="mongodb://database/scenarios"

#ENV OAUTH2_CLIENTID         ="account" \
#    OAUTH2_CLIENTSECRET     ="secretsecret" \
#    OAUTH2_AUTHORIZATIONURL ="authurltulr" \
#    OAUTH2_TOKENURL         ="blablabla"

EXPOSE 8080
CMD [ "npm", "start" ]
