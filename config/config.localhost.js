module.exports = {
  host :        process.env.WEB_HOST         || 'https://scenarios.synchronicity-iot.eu',
  port :    int(process.env.WEB_PORT)        || 80,
  contextPath : process.env.WEB_CONTEXTPATH  || '/',
  dev :    bool(process.env.WEB_DEV)         || false,
  title :       process.env.WEB_TITLE        || 'OrganiCity Scenarios'
};

console.log('Config loaded: config.js');
console.log(JSON.stringify(module.exports, null, 2));
console.log('End of config');

function bool(str) {
  if (str === void 0) {return false;}
  return str.toLowerCase() === 'true';
}

function int(str) {
  if (!str) {return 0;}
  return parseInt(str, 10);
}

function float(str) {
  if (!str) {return 0;}
  return parseFloat(str, 10);
}
