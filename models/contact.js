
/*
 * This module defines a set of constants to get in touch with the organicity
 * team.
 */

module.exports = {
  mailAddress: 'info@synchronicity-iot.eu'
};
